# Scripts Shell pour Debian 9

*Ce script est en Beta, il est possible qu'il y ait des problèmes lors de son utilisation. La destruction de votre PC par les flammes est seulement optionnelle.*

## Étape 01 : récupérer le dépôt *scripts-shell-debian-9*

**Téléchargez l'archive** *scripts-shell-debian-9* sur [GitHub](https://github.com/ewilan-riviere/scripts-shell-debian-9)  

## Étape 02 : lancer le script d'installation *root*

Ouvrir le terminal et entrez les commandes suivantes :
```bash
cd ~/Téléchargements/scripts-shell-debian-9-master


# Sous Debian

su

chmod +x install.sh

./install.sh


# Sous Ubuntu

sudo chmod +x install.sh

sudo ./install.sh

```

> La commande **su** vous demandera un mot de passe, c'est celui du root. Si vous ne savez pas quel est ce mot de passe, appelez l'admin système.  
> 
> **Astuce** : pour coller une commande dans le terminal, faites un *Ctrl+Shift+V*

Après la mise à jour de votre système, vous aurez le choix de choisir l'installation de plusieurs paquets. **Appuyez sur la touche *Espace* pour sélectionner un paquet** et **naviguez entre les paquets avec les *flèches directionnelles* du clavier**.  

**Validez votre choix avec *Entrée*** (ou utilisez Tabulation et Entrée pour annuler).

## Étape 03 : lancer le script d'installation *user*

### **Pour Debian**

---

Entrez la commande suivante :
```bash
visudo
```

Une interface d'édition vous sera proposée, descendez jusqu'à ce que vous trouviez la ligne *User privilge specification*
```bash
# User privilege specification
root    ALL=(ALL:ALL) ALL
```
Ajoutez-y votre nom d'utilisateur ou d'utilisatrice comme ceci :
```
<username>    ALL=(ALL:ALL) ALL
```
> Utilisez la touche Tabulation pour faire l'espace entre votre *username* et les droits.

Vous aurez donc ceci (*user* étant remplacé par **votre nom d'utilisateur ou d'utilisatrice**) :
```bash
# User privilege specification
root    ALL=(ALL:ALL) ALL
user    ALL=(ALL:ALL) ALL
```
Si vous utilisez nano, validez avec *Ctrl+X*, appuyez sur *O* avant d'enregistrer avec *Entrée*.
Pour vim, appuyez sur Échap, puis sur **:** puis **x**.

---

### **Pour toutes les distributions**
*Ce script est en Alpha, utilise-le avec grande circonspection. OK, vas-y. Ferme les yeux avant de lancer la commande.

**Lancez le script avec les commandes suivantes** :
```bash
# Sous Debian pour retourner en utilisateur
exit

./main_no_root.sh
```

Votre Debian est configuré !

# Détails des scripts
**`check_package.sh`**  
*Vérifie l'installation des paquets*

**`install.sh`**  
*Vérifie que l'utilisateurice est déjà en **su** avant de lancer le script `main.sh`*

**`main.sh`**  
*Installe plusieurs paquets importants pour Debian qui doivent être installés en superutilisateur*  

**`main_no_root.sh`**  
*Installe plusieurs paquets importants pour Debian qui doivent être installés en utilisateur*
