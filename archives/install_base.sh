#!/bin/sh
# su

echo "
 ____ ___      .__                                             
|    |   \____ |__| ____  ___________  ____                    
|    |   /    \|  |/ ___\/  _ \_  __ \/    \                   
|    |  /   |  \  \  \__(  <_> )  | \/   |  \                  
|______/|___|  /__|\___  >____/|__|  |___|  /                  
             \/        \/                 \/                   
  _________            .__        __                           
 /   _____/ ___________|__|______/  |_                         
 \_____  \_/ ___\_  __ \  \____ \   __\                        
 /        \  \___|  | \/  |  |_> >  |                          
/_______  /\___  >__|  |__|   __/|__|                          
        \/     \/         |__|                                 
  _________.__           .__  .__                              
 /   _____/|  |__   ____ |  | |  |                             
 \_____  \ |  |  \_/ __ \|  | |  |                             
 /        \|   Y  \  ___/|  |_|  |__                           
/_______  /|___|  /\___  >____/____/                           
        \/      \/     \/
"

# Linuxbrew
if which brew > /dev/null
    then
        echo "🦄 brew est déjà installé, installation suivante"
    else
        apt-get install build-essential curl file git
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
        test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
        test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
        test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
        echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile
        echo "🦄 brew est installé"
    fi

# NPM
curl -sL https://deb.nodesource.com/setup_10.x | -E bash -
apt install -y nodejs
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
export PATH=~/.npm-global/bin:$PATH >>~/.profile
source ~/.profile

# Cleaning
yes | apt autoremove
apt autoclean

if which mplayer > /dev/null
    then
        echo "🦄 mplayer est déjà installé, installation suivante"
    else
        yes | apt install mplayer
        echo "🦄 mplayer est installé"
    fi

echo "
  _________            .__        __     
 /   _____/ ___________|__|______/  |_   
 \_____  \_/ ___\_  __ \  \____ \   __\  
 /        \  \___|  | \/  |  |_> >  |    
/_______  /\___  >__|  |__|   __/|__|    
        \/     \/         |__|           
___________.__       .__                 
\_   _____/|__| ____ |__|                
 |    __)  |  |/    \|  |                
 |     \   |  |   |  \  |                
 \___  /   |__|___|  /__|                
     \/            \/                    
                                         
                                         
                                         
                                         
                                         
                                         
__________                               
\______   \_____ _______                 
 |     ___/\__  \\_  __ \                
 |    |     / __ \|  | \/                
 |____|    (____  /__|                   
                \/                       
___________       .__.__                 
\_   _____/_  _  _|__|  | _____    ____  
 |    __)_\ \/ \/ /  |  | \__  \  /    \ 
 |        \\     /|  |  |__/ __ \|   |  \
/_______  / \/\_/ |__|____(____  /___|  /
        \/                     \/     \/
"
