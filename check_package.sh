#!/bin/bash

CHECK="\e[1mVérification du paquet...\033[0m"
INSTALLED="\e[1;32m🦄 Le paquet est installé\033[0m\n"
NOTINSTALLED="\e[1;31m🦄 Le paquet n'est pas installé\033[0m\n"
AKA="\e[3maka"
LINUXV=""

cat /etc/issue.net
echo -e "Ce script est prévu pour Debian 9\n"

# APP="TwitterColorEmoji"
# DESC=""

# dpkg -s $APP &> /dev/null

# if [ $? -eq 0 ]; then
#     echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
#     echo -e "$INSTALLED"
# else
#     echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
#     echo -e "$NOTINSTALLED"
# fi

APP="git"
DESC="un logiciel de gestion de versions décentralisé"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="sudo"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="dirmngr"
DESC="un serveur de gestion et téléchargement de certificats"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="discord"
DESC="un logiciel gratuit de VoIP"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="code"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="curl"
DESC="une interface en ligne de commande"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="apache2"
DESC="un serveur HTTP"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="mariadb-server"
DESC="un système de gestion de base de données"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="php7.0"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="php7.3"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="phpmyadmin"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="nodejs"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="npm"
DESC=""

$APP -v > /dev/null 2>&1
if [[ $? -ne 0 ]]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
fi

APP="composer"
DESC=""

$APP -v > /dev/null 2>&1
if [[ $? -ne 0 ]]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
fi

APP="firmware-ralink"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="nvidia-driver"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="steam"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="spotify-client"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="redshift"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="nethogs"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="texlive"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="zsh"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

APP="gitkraken"
DESC=""

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
fi

# APP="coucou"
# DESC=""

# dpkg -s $APP &> /dev/null

# if [ $? -eq 0 ]; then
#     echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
#     echo -e "$INSTALLED"
# else
#     echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
#     echo -e "$NOTINSTALLED"
# fi

# APP="coucou_package_all"
# DESC=""

# $APP -v > /dev/null 2>&1
# if [[ $? -ne 0 ]]; then
#     echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
#     echo -e "$NOTINSTALLED"
# else
#     echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
#     echo -e "$INSTALLED"
# fi