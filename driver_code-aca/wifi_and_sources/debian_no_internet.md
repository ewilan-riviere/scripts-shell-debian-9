# Installer le driver Wi-Fi pour la carte BCM4312

## Entrer les commandes suivantes en su dans le dossier de ce tutoriel
```bash
tar xjf no_net_install_bcm43xx_firmware.tar.bz2
cd bcm43xx_firmware/
./install_bcm43xx_firmware_no_net
```

## Mettre à jour les *sources.list*
```bash
cd /etc/apt/
nano sources.list
```

Copiez-collez le contenu suivant après avoir effacé le contenu des *sources.list* :
```bash
deb http://deb.debian.org/debian stretch main
deb-src http://deb.debian.org/debian stretch main

deb http://deb.debian.org/debian-security/ stretch/updates main
deb-src http://deb.debian.org/debian-security/ stretch/updates main

deb http://deb.debian.org/debian stretch-updates main
deb-src http://deb.debian.org/debian stretch-updates main
```
