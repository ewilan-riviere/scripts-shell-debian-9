#!/bin/sh

APP="firmware-realtek"

if which $APP > /dev/null
then
    echo "🦄 Le paquet $APP est déjà installé"
else
    echo "🦄 Le paquet $APP n'est pas installé, installation en cours..."
    apt install $APP
fi

APP="firmware-ralink"

if which $APP > /dev/null
then
    echo "🦄 Le paquet $APP est déjà installé"
else
    echo "🦄 Le paquet $APP n'est pas installé, installation en cours..."
    apt install $APP
fi

APP="nvidia-driver"

if which $APP > /dev/null
then
    echo "🦄 Le paquet $APP est déjà installé"
else
    echo "🦄 Le paquet $APP n'est pas installé, installation en cours..."
    echo "# nvidia-driver\ndeb http://httpredir.debian.org/debian/ stretch main contrib non-free" >> /etc/apt/sources.list
    apt-get update
    apt install linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') nvidia-driver
fi

APP="steam"

if which $APP > /dev/null
then
    echo "🦄 Le paquet $APP est déjà installé"
else
    echo "🦄 Le paquet $APP n'est pas installé, installation en cours..."
    echo "# steam\ndeb http://httpredir.debian.org/debian/ jessie main contrib non-free
	dpkg --add-architecture i386" >> /etc/apt/sources.list
    apt update
    apt install steam
    apt install libgl1-nvidia-glx:i386
fi

APP="spotify-client"

if which $APP > /dev/null
then
    echo "🦄 Le paquet $APP est déjà installé"
else
    echo "🦄 Le paquet $APP n'est pas installé, installation en cours..."
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
    echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
    apt-get update
    apt-get install spotify-client
fi

APP="redshift"

if which $APP > /dev/null
then
    echo "🦄 Le paquet $APP est déjà installé"
else
    echo "🦄 Le paquet $APP n'est pas installé, installation en cours..."
    apt install redshift
fi