#!/bin/bash

if [ "$EUID" -ne 0 ]
    then
        echo -e "\e[1mVérification de l'utilisateurice\033[0m : \e[1m$USER\033[0m détecté·e"
        echo ""
        echo -e "\e[1;31mERREUR\033[0m"
        echo -e "Ce script demande d'être en root, entrez la commande \e[1;31msu\033[0m et entrer votre mot de passe avant de relancer le script shell"
        exit
    else
        echo "Vérification... Utilisateur root détecté, autorisation de continuer..."
        cd ..
        chmod +x -R scripts-shell-debian-9/
        cd scripts-shell-debian-9/
        ./main.sh
fi