#!/bin/bash

DATABASE="projet"
USER_PMA="root"
PASSWORD_PMA="password"

composer install
echo "Création du fichier .env..."
cp .env.example .env
cat .env | sed -i "s/DB_DATABASE=homestead/DB_DATABASE=$DATABASE/g" .env
cat .env | sed -i "s/DB_USERNAME=homestead/DB_USERNAME=$USER_PMA/g" .env
cat .env | sed -i "s/DB_PASSWORD=secret/DB_PASSWORD=$PASSWORD_PMA/g" .env
php artisan key:generate
php artisan migrate
php artisan serve
