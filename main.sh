#!/bin/bash

CHECK="\e[1mVérification du paquet...\033[0m"
INSTALLED="\e[1;32m🦄 Le paquet est installé\033[0m\n"
NOTINSTALLED="\e[1;31m🦄 Le paquet n'est pas installé\033[0m\n"
AKA="\e[3maka"
LINUXV=""

APP_GIT="git"
DESC_GIT="un logiciel de gestion de versions décentralisé"

APP_GITKRAKEN="gitkraken"
DESC_GITKRAKEN="un client graphique pour gérer ses dépôts git"

APP_SUDO="sudo"
DESC_SUDO="une commande bash permettant d'avoir les droits root"

APP_DIRMNGR="dirmngr"
DESC_DIRMNGR="un serveur de gestion et téléchargement de certificats"

APP_CODE="code"
DESC_CODE="un IDE extensible développé par Microsoft pour Windows, Linux et macOS"

APP_CURL="curl"
DESC_CURL="une interface en ligne de commande"

APP_APACHE2="apache2"
DESC_APACHE2="un serveur HTTP"

APP_MARIADB="mariadb-server"
DESC_MARIADB="un système de gestion de base de données"

APP_PHP7_0="php7.0"
DESC_PHP7_0="un langage de programmation"

APP_PHP7_3="php7.3"
DESC_PHP7_3="un langage de programmation"

APP_NODEJS="nodejs"
DESC_NODEJS="une plateforme logicielle libre et événementielle en JavaScript"

APP_NPM="npm"
DESC_NPM="un gestionnaire de paquets"

APP_COMPOSER="composer"
DESC_COMPOSER="un gestionnaire de dépendances libre écrit en PHP"

APP_MPLAYER="mplayer"
DESC_MPLAYER="une application permettant de lire de la musique"

APP_EMOJI="TwitterColorEmoji"
DESC_EMOJI="une collection d'emoji"

APP_ZSH="zsh"
DESC_ZSH="un terminal amélioré"

APP_DISCORD="discord"
DESC_DISCORD="un logiciel gratuit de VoIP"

APP_REALTEK="firmware-realtek"
DESC_REALTEK=""

APP_RALINK="firmware-ralink"
DESC_RALINK=""

APP_NVIDIA="nvidia-driver"
DESC_NVIDIA=""

APP_STEAM="steam"
DESC_STEAM=""

APP_SPOTIFY="spotify-client"
DESC_SPOTIFY=""

APP_REDSHIFT="redshift"
DESC_REDSHIFT=""

cat /etc/issue.net
echo -e "Ce script est prévu pour Debian 9\n"

apt-get -y update
apt-get -y upgrade
apt-get -y dist-upgrade

apt install -y dialog

cmd=(dialog --backtitle "Choisissez les paquets que vous souhaitez installer" --title "Installateur de paquets" --separate-output --checklist "Parcourez les paquets avec les flèches directionnelles, sélectionnez avec Espace et validez avec Entrée" 22 76 16)
options=(
    # any option can be set to default to "on"
    1 "$APP_GIT" off
    2 "$APP_GITKRAKEN" off
    3 "$APP_SUDO" off
    4 "$APP_DIRMNGR" off
    5 "$APP_CODE" off
    6 "$APP_CURL" off
    7 "$APP_APACHE2" off
    8 "$APP_MARIADB" off
    9 "$APP_PHP7_0" off
    10 "$APP_PHP7_3" off
    11 "$APP_NODEJS" off
    12 "$APP_NPM" off
    13 "$APP_COMPOSER" off
    14 "$APP_MPLAYER" off
    15 "$APP_EMOJI" off
    16 "$APP_ZSH" off
    17 "$APP_DISCORD" off
    )

choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
        1)
            # echo "$APP_GIT, $DESC_GIT"
            dpkg -s $APP_GIT &> /dev/null
            
            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_GIT\e[0m $AKA $DESC_GIT\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_GIT\e[0m $AKA $DESC_GIT\033[0m"
                echo -e "$NOTINSTALLED"
                apt-get install -y git
            fi
            ;;
        2)
            # echo "$APP_GITKRAKEN, $DESC_GITKRAKEN"
            dpkg -s $APP_GITKRAKEN &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_GITKRAKEN\e[0m $AKA $DESC_GITKRAKEN\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_GITKRAKEN\e[0m $AKA $DESC_GITKRAKEN\033[0m"
                echo -e "$NOTINSTALLED"
                wget -q https://www.gitkraken.com/download/linux-deb -O gitkraken.deb
                apt install -y gconf2
                dpkg -i gitkraken.deb
            fi
            ;;
        3)
            # echo "$APP_SUDO, $DESC_SUDO"
            dpkg -s $APP_SUDO &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_SUDO\e[0m $AKA $DESC_SUDO\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_SUDO\e[0m $AKA $DESC_SUDO\033[0m"
                echo -e "$NOTINSTALLED"
                apt-get install -y sudo
            fi
            ;;
        4)
            # echo "$APP_DIRMNGR, $DESC_DIRMNGR"
            dpkg -s $APP_DIRMNGR &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_DIRMNGR\e[0m $AKA $DESC_DIRMNGR\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_DIRMNGR\e[0m $AKA $DESC_DIRMNGR\033[0m"
                echo -e "$NOTINSTALLED"
                apt-get install -y dirmngr
            fi
            ;;
        5)
            # echo "$APP_CODE, $DESC_CODE"
            dpkg -s $APP_CODE &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_CODE\e[0m $AKA $DESC_CODE\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_CODE\e[0m $AKA $DESC_CODE\033[0m"
                echo -e "$NOTINSTALLED"
                apt install -y software-properties-common apt-transport-https curl
                touch /etc/apt/sources.list.d/vscode.list
                curl -sSL https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
                echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" >> /etc/apt/sources.list.d/vscode.list
                apt update
                apt install -y code
            fi
            ;;
        6)
            # echo "$APP_CURL, $DESC_CURL"
            dpkg -s $APP_CURL &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_CURL\e[0m $AKA $DESC_CURL\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_CURL\e[0m $AKA $DESC_CURL\033[0m"
                echo -e "$NOTINSTALLED"
                apt install -y curl
            fi
            ;;
        7)
            # echo "$APP_APACHE2, $DESC_APACHE2"
            dpkg -s $APP_APACHE2 &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_APACHE2\e[0m $AKA $DESC_APACHE2\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_APACHE2\e[0m $AKA $DESC_APACHE2\033[0m"
                echo -e "$NOTINSTALLED"
                apt install -y apache2
                apt install ufw
                ufw enable
                ufw app list
                ufw app info "WWW Full"
                ufw allow 443
                ufw allow 80
                curl http://icanhazip.com
                rm /var/www/html/index.html
            fi
            ;;
        8)
            # echo "$APP_MARIADB, $DESC_MARIADB"
            dpkg -s $APP_MARIADB &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_MARIADB\e[0m $AKA $DESC_MARIADB\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_MARIADB\e[0m $AKA $DESC_MARIADB\033[0m"
                echo -e "$NOTINSTALLED"
                apt install -y mariadb-server
            fi
            ;;
        9)
            # echo "$APP_PHP7_0, $DESC_PHP7_0"
            dpkg -s $APP_PHP7_0 &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_PHP7_0\e[0m $AKA $DESC_PHP7_0\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_PHP7_0\e[0m $AKA $DESC_PHP7_0\033[0m"
                echo -e "$NOTINSTALLED"
                apt install -y php libapache2-mod-php php-mysql
                sed -i '2s/.*/      DirectoryIndex index.php index.html index.cgi index.pl index.xhtml inde$/' /etc/apache2/mods-enabled/dir.conf
                systemctl restart apache2
                apt install php-cli
                echo "<?php\n\tphpinfo();\n?>" >> /var/www/html/info.php
            fi
            ;;
        10)
            # echo "$APP_PHP7_3, $DESC_PHP7_3"
            dpkg -s $APP_PHP7_3 &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_PHP7_3\e[0m $AKA $DESC_PHP7_3\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_PHP7_3\e[0m $AKA $DESC_PHP7_3\033[0m"
                echo -e "$NOTINSTALLED"
                apt-get -y install apt-transport-https lsb-release ca-certificates
                wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
                sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
                apt-get update
                apt -y upgrade
                a2dismod php7.0
                a2enmod php7.3
                service apache2 restart
            fi
            ;;
        11)
            # echo "$APP_NODEJS, $DESC_NODEJS"
            dpkg -s $APP_NODEJS &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_NODEJS\e[0m $AKA $DESC_NODEJS\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_NODEJS\e[0m $AKA $DESC_NODEJS\033[0m"
                echo -e "$NOTINSTALLED"
                apt-get install curl software-properties-common
                curl -sL https://deb.nodesource.com/setup_11.x | bash -
                apt-get install nodejs
            fi
            ;;
        12)
            # echo "$APP_NPM, $DESC_NPM"
            $APP_NPM -v > /dev/null 2>&1
            if [[ $? -ne 0 ]]; then
                echo -e "\e[1m$APP_NPM\e[0m $AKA $DESC_NPM\033[0m"
                echo -e "$NOTINSTALLED"
            else
                echo -e "\e[1m$APP_NPM\e[0m $AKA $DESC_NPM\033[0m"
                echo -e "$INSTALLED"
            fi
            ;;
        13)
            # echo "$APP_COMPOSER, $DESC_COMPOSER"
            $APP_COMPOSER -v > /dev/null 2>&1

            if [[ $? -ne 0 ]]; then
                echo -e "\e[1m$APP_COMPOSER\e[0m $AKA $DESC_COMPOSER\033[0m"
                echo -e "$NOTINSTALLED"
                php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
                php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
                php composer-setup.php
                php -r "unlink('composer-setup.php');"
                mv composer.phar /usr/local/bin/composer
                composer
            else
                echo -e "\e[1m$APP_COMPOSER\e[0m $AKA $DESC_COMPOSER\033[0m"
                echo -e "$INSTALLED"
            fi
            ;;
        14)
            # echo "$APP_MPLAYER, $DESC_MPLAYER"
            dpkg -s $APP_MPLAYER &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_MPLAYER\e[0m $AKA $DESC_MPLAYER\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_MPLAYER\e[0m $AKA $DESC_MPLAYER\033[0m"
                echo -e "$NOTINSTALLED"
                apt install -y mplayer
            fi
            ;;
        15)
            # echo "$APP_EMOJI, $DESC_EMOJI"
            dpkg -s $APP_EMOJI &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_EMOJI\e[0m $AKA $DESC_EMOJI\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_EMOJI\e[0m $AKA $DESC_EMOJI\033[0m"
                echo -e "$NOTINSTALLED"
                wget https://github.com/eosrei/twemoji-color-font/releases/download/v11.2.0/TwitterColorEmoji-SVGinOT-Linux-11.2.0.tar.gz
                tar zxf TwitterColorEmoji-SVGinOT-Linux-11.2.0.tar.gz
                cd TwitterColorEmoji-SVGinOT-Linux-11.2.0
                ./install.sh
            fi
            ;;
        16)
            # echo "$APP_ZSH, $DESC_ZSH"
            dpkg -s $APP_ZSH &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_ZSH\e[0m $AKA $DESC_ZSH\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_ZSH\e[0m $AKA $DESC_ZSH\033[0m"
                echo -e "$NOTINSTALLED"
                apt -y install zsh
                chsh -s $(which zsh)
                npm install -g spaceship-prompt

                git clone https://github.com/powerline/fonts.git --depth=1
                cd fonts
                ./install.sh
                cd ..
                rm -rf fonts
            fi
            ;;
        17)
            # echo "$APP_DISCORD, $DESC_DISCORD"
            dpkg -s $APP_DISCORD &> /dev/null

            if [ $? -eq 0 ]; then
                echo -e "\e[1m$APP_DISCORD\e[0m $AKA $DESC_DISCORD\033[0m"
                echo -e "$INSTALLED"
            else
                echo -e "\e[1m$APP_DISCORD\e[0m $AKA $DESC_DISCORD\033[0m"
                echo -e "$NOTINSTALLED"
                wget "https://discordapp.com/api/download?platform=linux&format=deb" -O /tmp/discord.deb && apt install /tmp/discord.deb
            fi
            ;;
    esac
done

# L'option remove permet de désinstaller les paquets indiqués. Ceci laisse toutefois en place les fichiers de configuration de ces paquets.
# apt-get -y remove <paquets(s)>

# L'option autoremove permet de désinstaller les paquets indiqués avec leurs dépendances logicielles. Ceci laisse toutefois en place les fichiers de configuration de ces paquets.
# apt-get -y autoremove <paquets(s)>

# L'action purge, passée à la commande apt-get, désinstalle les paquets indiqués et leurs fichiers de configuration 1). Les fichiers de préférences2), et les logs3) ne sont pas supprimés. 
# apt-get -y purge <paquets(s)>

# L'option --purge, passée à la commande autoremove, désinstalle les paquets indiqués, les dépendances logicielles et leurs fichiers de configuration 4). Les fichiers de préférences5), et les journaux (logs) 6) ne sont pas supprimés.
# apt-get -y autoremove --purge <paquets(s)>

apt -y autoremove

# L'option clean, option radicale, supprime la totalité des paquets présents dans /var/cache/apt/archives. (Notez que cela n'a aucun impact sur les paquets installés.) 
apt-get -y clean

# L'option autoclean permet de supprimer les paquets présents dans /var/cache/apt/archives , mais en conservant ceux qui ont un équivalent dans les dépôts; par exemple, si /var/cache/apt/archives contient plusieurs versions du même logiciel, les plus anciennes, plus présentes dans les dépôts, seront supprimées, mais la version à jour (ayant un équivalent dans le dépôt) sera conservée. Ceci vous permet de récupérer beaucoup d'espace disque, mais moins toutefois qu'avec "clean". 
apt-get -y autoclean