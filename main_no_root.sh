#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

CHECK="\e[1mVérification du paquet...\033[0m"
INSTALLED="\e[1;32m🦄 Le paquet est installé\033[0m\n"
NOTINSTALLED="\e[1;31m🦄 Le paquet n'est pas installé\033[0m\n"
AKA="\e[3maka"
LINUXV=""

chown -R $USER:www-data /var/www/html/

echo 'export PATH=~/.npm-global/bin:$PATH' >> ~/.profile
source ~/.profile
npm install -g jshint
npm i -g @vue/cli

composer global require laravel/installer
echo 'export PATH=~/.config/composer/vendor/bin:$PATH' >> ~/.profile
source ~/.profile

mkdir ~/.npm-global
npm config set prefix '~/.npm-global'

APP="postman"
DESC=""

echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
echo -e "$NOTINSTALLED"
cd /tmp || exit
wget -q https://dl.pstmn.io/download/latest/linux?arch=64 -O postman.tar.gz
tar -xzf postman.tar.gz
rm postman.tar.gz

if [ -d "~/Postman" ];then
    rm -rf ~/Postman
fi
mv Postman ~/postman

if [ -L "/usr/bin/postman" ];then
    ${SUDO} rm -f /usr/bin/postman
fi
${SUDO} ln -s ~/Postman/Postman /usr/bin/postman

${SUDO} touch "Postman.desktop"
echo "[Desktop Entry]\nEncoding=UTF-8\nName=Postman\nExec=postman\nIcon=/home/$USER/Postman/app/resources/app/assets/icon.png\nTerminal=false\nType=Application\nCategories=Development;"

${SUDO} mv Postman.desktop /usr/share/applications

apt-get install -y build-essential make cmake scons curl git \ruby autoconf automake autoconf-archive \gettext libtool flex bison \libbz2-dev libcurl4-openssl-dev \libexpat-dev libncurses-dev

git clone https://github.com/Homebrew/linuxbrew.git ~/.linuxbrew

touch ~/.profile && echo '# Until LinuxBrew is fixed, the following is required.' >> ~/.profile && echo '# See: https://github.com/Homebrew/linuxbrew/issues/47' >> ~/.profile && echo 'export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib64/pkgconfig:/usr/share/pkgconfig:$PKG_CONFIG_PATH' >> ~/.profile && echo '## Setup linux brew' >> ~/.profile && echo 'export LINUXBREWHOME=$HOME/.linuxbrew' >> ~/.profile && echo 'export PATH=$LINUXBREWHOME/bin:$PATH' >> ~/.profile && echo 'export MANPATH=$LINUXBREWHOME/man:$MANPATH' >> ~/.profile && echo 'export PKG_CONFIG_PATH=$LINUXBREWHOME/lib64/pkgconfig:$LINUXBREWHOME/lib/pkgconfig:$PKG_CONFIG_PATH' >> ~/.profile && echo 'export LD_LIBRARY_PATH=$LINUXBREWHOME/lib64:$LINUXBREWHOME/lib:$LD_LIBRARY_PATH' >> ~/.profile

mysql_secure_installation

APP="phpmyadmin"
DESC="une application Web de gestion les base de données MySQL"

dpkg -s $APP &> /dev/null

if [ $? -eq 0 ]; then
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$INSTALLED"
else
    echo -e "\e[1m$APP\e[0m $AKA $DESC\033[0m"
    echo -e "$NOTINSTALLED"
    apt update
    phpenmod mbstring
    systemctl restart apache2
fi