#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi
${SUDO} nethogs

if pgrep firefox >/dev/null
then
    echo "Firefox est déjà lancé"
else
    echo "Lancement de Firefox en cours..."
    /usr/bin/firefox &
fi

if pgrep -x evolution >/dev/null
then
    echo "Evolution est déjà lancé"
else
    echo "Lancement de Evolution en cours..."
    /usr/bin/evolution &
fi

if pgrep -x discord >/dev/null
then
    echo "Discord est déjà lancé"
else
    echo "Lancement de Discord en cours..."
    discord &
fi

if pgrep -x code >/dev/null
then
    echo "Code est déjà lancé"
else
    echo "Lancement de Code en cours..."
    /usr/bin/code &
fi

if pgrep -x gitkraken >/dev/null
then
    echo "GitKraken est déjà lancé"
else
    echo "Lancement de GitKraken en cours..."
    /usr/bin/gitkraken &
fi

return
exit 0