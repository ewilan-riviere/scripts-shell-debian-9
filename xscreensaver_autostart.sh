#!/bin/bash

touch ~/Xscreensaver.desktop

echo "#!/usr/lib/env/ xdg-open\n\n[Desktop Entry]\nVersion=1.0\nType=Application\nTerminal=false\nIcon[en-US]=gnome-panel-launcher\nIcon=gnome-panel-launcher\nExec=xscreensaver\nName[en-US]=Xscreensaver.desktop\nName=Xscreensaver" > ~/.config/autostart/Xscreensaver.desktop
